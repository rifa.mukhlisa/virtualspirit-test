// Header component
import { Menu } from 'lucide-react'
import { useMemo } from 'react'
import { Link } from 'react-router-dom'

import { useScroll } from '../../hooks/document'
import './Header.css'

function Header() {
  const { fromTop } = useScroll()

  const isStick = useMemo(() => {
    return fromTop > 40 // mt-10
  }, [fromTop])

  return (
    <header data-testid="header" className={`header${isStick ? ' is-stick' : ''}`}>
      <div className="container">
        <div className='header__logo'>
          <Link to={'/'}>
            <img src="/logoipsum.svg" alt="logo" />
          </Link>
        </div>

        <ul className="header__nav">
          <li>
            <a href="#" className="hidden md:block font-medium text-primary">
              GET FREE NOW!
            </a>
          </li>
          <li>
            <a href="#" className="hidden md:block">Premium</a>
          </li>
          <li>
            <a href="#" className="hidden md:block">
              Support
            </a>
          </li>
          <li className="to-right lg:hidden">
            <a href="#">
              <Menu />
            </a>
          </li>
          <li className="to-right hidden lg:block">
            <a href="#">Login</a>
          </li>
          <li className="hidden lg:block">
            <a
              href="#"
              className="bg-[#daa118] px-4 py-2 rounded-full text-white hover:!text-white"
            >
              Register
            </a>
          </li>
        </ul>
      </div>
    </header>
  )
}

export default Header

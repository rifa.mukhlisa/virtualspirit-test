// MovieGrid component
// display movie with grid layout
import { Heart } from 'lucide-react'
import { Link } from 'react-router-dom'
import { Movies } from '../../entities/movie'
import './MovieGrid.css'

// MovieGridProps interface
interface MovieGridProps {
  movies: Movies
}

function MovieGrid({ movies = [] }: MovieGridProps) {
  return (
    <div data-testid="movie-grid" className="movie-grid">
      {movies.map((val, index) => (
        <Link data-testid="movie-grid-item" key={`grid-item-movie-${index}`} to={`/movies/${val.id}`}>
          <img src={val.poster_path} alt="" />
          <div className="movie-grid__description">
            <div className="inline-flex items-center space-x-4 p-2 bg-white rounded-md">
              <Heart className="w-6 h-6 text-red-500" />
            </div>
            <h5 data-testid="movie-title">
              {val.title}
            </h5>
          </div>
        </Link>
      ))}
    </div>
  )
}

export default MovieGrid;

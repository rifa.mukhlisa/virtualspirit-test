// Hero component
// hero component with parallax background
import './Hero.css'

function Hero() {
  return (
    <div data-testid="landing-hero" className="landing-hero">
      <div className="container">
        <h1 data-testid="h1-text">
          Your Gateway to Cinematic Adventures
        </h1>
        <p data-testid="hero-text">
          Experience the future of entertainment, where movie magic comes to life with every click, making your cinematic journey truly extraordinary.
        </p>
      </div>
    </div>
  )
}

export default Hero

import { render, screen } from '@testing-library/react';
import Hero from './Hero';

// Test case
test('renders Hero component', () => {
  render(<Hero />);

  // Check if Hero component renders correctly
  const heroElement = screen.getByTestId('landing-hero');
  expect(heroElement).toBeInTheDocument();

  // Check if heading and paragraph are present
  const headingElement = screen.getByTestId('h1-text');
  const paragraphElement = screen.getByTestId('hero-text');

  expect(headingElement.innerHTML).toEqual('Your Gateway to Cinematic Adventures');
  expect(paragraphElement.innerHTML).toEqual('Experience the future of entertainment, where movie magic comes to life with every click, making your cinematic journey truly extraordinary.');
});

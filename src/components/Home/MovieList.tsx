// MovieList component
// display movie with list layout
import { Heart } from 'lucide-react'
import { Link } from 'react-router-dom'

import { Movies } from '../../entities/movie'
import './MovieList.css'

// MovieListProps interface
interface MovieListProps {
  movies: Movies
}

function MovieList({ movies = [] }: MovieListProps) {
  return (
    <div data-testid="movie-list" className="movie-list container">
      {movies.map((val, index) => (
        <Link data-testid="movie-list-item" key={`list-item-movie-${index}`} to={`/movies/${val.id}`}>
          <img src={val.poster_path} alt={val.title} />
          <div className="movie-list__description">
            <h5>
              {val.title}
            </h5>
            <p>{val.overview}</p>
            <div className="flex items-center">
              <button
                type="button"
                className="inline-flex items-center p-2 bg-white rounded-md"
              >
                <Heart className="w-6 h-6 text-red-500" />
              </button>
            </div>
          </div>
        </Link>
      ))}
    </div>
  )
}

export default MovieList

import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import MovieList from './MovieList';

// Mock Movies data
const mockMovies = [
  {
    id: 1,
    backdrop_path: '/tmU7GeKVybMWFButWEGl2M4GeiP.jpg',
    original_title: 'The Godfather',
    overview: 'Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.',
    poster_path: '/tmU7GeKVybMWFButWEGl2M4GeiP.jpg',
    title: 'The Godfather'
  },
  {
    id: 1,
    backdrop_path: '/tmU7GeKVybMWFButWEGl2M4GeiP.jpg',
    original_title: 'The Godfather',
    overview: 'Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.',
    poster_path: '/tmU7GeKVybMWFButWEGl2M4GeiP.jpg',
    title: 'The Godfather'
  },
];

// Test case
test('renders MovieList component', () => {
  render(
    <MemoryRouter>
      <MovieList movies={mockMovies} />
    </MemoryRouter>
  );

  // Check if MovieGrid renders correctly
  const movieGridElement = screen.getByTestId('movie-list');
  expect(movieGridElement).toBeInTheDocument();

  // Check if movie items are present
  const movieItems = screen.getAllByTestId('movie-list-item');
  expect(movieItems.length).toBe(mockMovies.length);
});
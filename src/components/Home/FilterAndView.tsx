// FilterAndView component
// search and change layout view of displayed data
import { Grid2x2, List, Search, X } from 'lucide-react'
import { useEffect, useMemo, useState } from 'react'

import { useQuery } from '../../hooks/query'
import './FilterAndView.css'

function FilterAndView() {
  // Variables & States
  // get query object and insertQuery function
  const { query, insertQuery } = useQuery()
  // state for keywords
  const [keywords, setKeywords] = useState<string>(query.keywords || '')

  // Update data based on keywords
  useEffect(() => {
    if (keywords) {
      insertQuery({ keywords })
    }
  }, [keywords])

  // Computes view validated by the type, also set fallback / default value.
  const view = useMemo(() => {
    return ['grid', 'list'].includes(query.view) ? query.view : 'grid'
  }, [query])

  // Function to handle view change
  function handleChangeView(view: string) {
    insertQuery({ view })
  }

  // Function to handle clear the keywords
  function handleClearKeywords() {
    setKeywords('')
    if (query.keywords) {
      delete query.keywords
    }
    insertQuery({ ...query })
  }

  return (
    <div className="filter-and-view">
      <div className="filter-and-view__search">
        <input
          type="text"
          placeholder="Search movie..."
          value={keywords}
          onChange={(e) => setKeywords(e.target.value)}
        />
        {keywords.length === 0 ? (
          <button type="submit">
            <Search className="w-5 h-5 text-stone-400" />
          </button>
        ) : (
          <button type="button" onClick={handleClearKeywords}>
            <X className="w-5 h-5 text-stone-900" />
          </button>
        )}
      </div>
      <div className="filter-and-view__view-toggle">
        <button
          data-testid="grid-button"
          type="button"
          aria-label="Grid View"
          title="Grid View"
          className={view === 'grid' ? 'active' : ''}
          onClick={() => handleChangeView('grid')}
        >
          <Grid2x2 className="w-6 h-6" />
        </button>
        <button
          data-testid="list-button"
          type="button"
          aria-label="List View"
          title="List View"
          className={view === 'list' ? 'active' : ''}
          onClick={() => handleChangeView('list')}
        >
          <List className="w-6 h-6" />
        </button>
      </div>
    </div>
  )
}

export default FilterAndView

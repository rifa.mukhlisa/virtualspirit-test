import { MovieDetailEntity } from '../../entities/movieDetailEntity';
import { formatDate } from '../../helpers/humanDate';
import { Play, Plus } from 'lucide-react';

// Movie Detail Props interface
interface MovieDetailProps {
  movieDetail: MovieDetailEntity
}
function MovieContent({ movieDetail }: MovieDetailProps) {
  return (
    <>
      <img data-testid="backdrop-image" className="h-screen w-full object-cover" src={movieDetail?.backdrop_path} alt={movieDetail.title} />
      <div className="flex items-center absolute top-0 pt-24 lg:pt-0 bg-gradient-to-b from-[rgba(0,0,0,.4)] via-opacity-65 to-black min-h-screen w-full px-5 lg:px-14">
        <div className="w-full lg:w-1/2">
          <div data-testid="movie-title" className="text-white text-2xl lg:text-4xl font-bold mb-8">{ movieDetail.title }</div>
          <div className="text-[#daa118] text-xs mb-4">
            {formatDate(movieDetail.release_date)} - Duration: {movieDetail.runtime} minutes
          </div>
          <div className="block md:flex mb-4">
            <a href="#" className="w-full md:w-[250px] h-12 md:h-14 leading-[56px] bg-[#daa118] rounded-lg text-center text-white text-lg mb-3 md:mb-0 mr-0 md:mr-3 flex justify-center items-center">
              <Play className="mr-2" />Watch Now
            </a>
            <a href="#" className="w-full md:w-[250px] h-12 md:h-14 leading-[56px] border border-[#daa118] rounded-lg text-center text-white text-lg mr-3 flex justify-center items-center">
              <Plus className="mr-2" />Add to watch list
            </a>
          </div>
          <div data-testid="movie-overview" className="text-white text-sm">{ movieDetail.overview }</div>
        </div>
      </div>
    </>
  )
}

export default MovieContent;
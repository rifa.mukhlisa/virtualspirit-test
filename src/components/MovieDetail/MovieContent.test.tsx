import { render, screen } from '@testing-library/react';
import MovieContent from './MovieContent';

const mockMovieDetail = {
  id: 1,
  backdrop_path: '/tmU7GeKVybMWFButWEGl2M4GeiP.jpg',
  overview: 'Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.',
  poster_path: '/tmU7GeKVybMWFButWEGl2M4GeiP.jpg',
  title: 'The Godfather',
  genres: [
    {
      id: 1,
      name: 'action'
    }
  ],
  runtime: 120,
  release_date: '20'
}


// Test case
test('renders Hero component', () => {
  render(<MovieContent movieDetail={mockMovieDetail} />);

  const backdropElement = screen.getByTestId('backdrop-image');
  expect(backdropElement).toBeInTheDocument();

  const titleElement = screen.getByTestId('movie-title');
  const overviewElement = screen.getByTestId('movie-overview');

  expect(titleElement.innerHTML).toEqual(mockMovieDetail.title);
  expect(overviewElement.innerHTML).toEqual(mockMovieDetail.overview);
});

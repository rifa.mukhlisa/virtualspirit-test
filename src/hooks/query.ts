// Query hook
// help to get and set the url query params
import { useEffect, useMemo, useState } from 'react'
import { useSearchParams } from 'react-router-dom'

interface HookQuery {
  prevQuery: any
  query: any
  insertQuery: Function
  updateQuery: Function
}

export function useQuery(): HookQuery {
  const [prevQuery, setPrevQuery] = useState<any>()
  const [searchParams, setSearchParams] = useSearchParams()

  useEffect(() => {
    setPrevQuery(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const query = useMemo(() => {
    return Object.fromEntries(searchParams.entries())
  }, [searchParams])

  function insertQuery(args: object) {
    setPrevQuery({ ...query })
    setSearchParams({ ...query, ...args })
  }
  function updateQuery(args: object) {
    setPrevQuery({ ...query })
    setSearchParams({ ...args })
  }

  return {
    prevQuery,
    query,
    insertQuery,
    updateQuery
  }
}

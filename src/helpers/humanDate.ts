export const formatDate = (dateString: string): string => {
  const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  const formattedDate: string = new Date(dateString).toLocaleDateString(undefined, options);
  return formattedDate;
};

// Movie entity
// interfaces and types collection for movie
export interface Movie {
  id: number
  backdrop_path: string
  original_title: string
  overview: string
  poster_path: string
  title: string
}

export type Movies = Array<Movie>

// Movie Detail entity
// interfaces for movieDetail

interface Genre {
  id: number;
  name: string;
}

export interface MovieDetailEntity {
  id: number;
  backdrop_path: string;
  overview: string;
  poster_path: string;
  title: string;
  genres: Genre[];
  runtime: number;
  release_date: string;
}

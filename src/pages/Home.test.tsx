import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Home from "./Home";
import { MemoryRouter, Route, Routes } from "react-router-dom";

function renderPage() {
  render(
    <MemoryRouter initialEntries={["/"]}>
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </MemoryRouter>
  );
}

describe("Home component", () => {
  it("renders Home component with default data", async () => {
    renderPage();
    const sections = ["section-hero", "section-view"];

    sections.forEach((val) => {
      expect(screen.getByTestId(val)).toBeInTheDocument();
    });
  });

  it("switches between grid and list views", async () => {
    renderPage();
    const listButton = screen.getByTestId("list-button");
    const gridButton = screen.getByTestId("grid-button");

    await waitFor(async () => {
      // Click on the 'List' button
      userEvent.click(listButton);

      // Check if the view is switched to 'list'
      expect(screen.getByTestId("movie-list")).toBeInTheDocument();
      expect(screen.queryByTestId("movie-grid")).not.toBeInTheDocument();
    });
    
    await waitFor(async () => {
      // Click on the 'Grid' button
      userEvent.click(gridButton);

      // Check if the view is switched back to 'grid'
      expect(screen.getByTestId("movie-grid")).toBeInTheDocument();
      expect(screen.queryByTestId("movie-list")).not.toBeInTheDocument();
    });
  });
});

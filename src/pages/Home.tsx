import { useEffect, useMemo, useState } from "react";
import Hero from "../components/Home/Hero";
import { Movies } from "../entities/movie";
import { useQuery } from "../hooks/query";
import MovieGrid from "../components/Home/MovieGrid";
import MovieList from "../components/Home/MovieList";
import FilterAndView from "../components/Home/FilterAndView";

function Home() {
  // state for movies
  const [movies, setMovies] = useState<Movies>([]);
  // state for error
  const [error, setError] = useState<String>();
  // get query object
  const { query } = useQuery();

  // Computes
  // sanitize the query keywords for used in filter movie
  const keywords = useMemo(() => {
    return (query.keywords || "").toString().trim().toLowerCase();
  }, [query]);

  // view validated by the type, also set fallback / default value.
  const view = useMemo(() => {
    return ["grid", "list"].includes(query.view) ? query.view : "grid";
  }, [query]);

  // filtered movies based on keywords
  const filteredMovies = useMemo(() => {
    return !!keywords
      ? movies.filter((val) => val.title.toLowerCase().includes(keywords))
      : movies;
  }, [movies, keywords]);

  // Initial hooks at first load only
  useEffect(() => {
    return () => {
      setMovies(movies);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Fetch movies by listen to page and limit
  useEffect(() => {
    fetchMovies();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // function for fetching movies
  async function fetchMovies() {
    try {
      // Read api url from env
      const baseUrl = process.env.REACT_APP_API_BASE_URL;
      const baseImageUrl = process.env.REACT_APP_IMAGE_BASE_URL;
      const apiToken = process.env.REACT_APP_API_TOKEN;

      const headers = {
        Authorization: `Bearer ${apiToken}`,
      };

      // Fetching data
      const response = await fetch(
        `${baseUrl}/movie/popular?language=en-US&page=1`,
        { headers }
      );
      const result = await response.json();

      // Make sure there is at least have 1 result
      if (result.results.length > 0) {
        // Map results to Movie entity
        const mappedMovies = result.results.map((value: any) => ({
          id: Number(value.id),
          backdrop_path: `${baseImageUrl}${value.backdrop_path}`,
          original_title: value.original_title,
          overview: value.overview,
          poster_path: `${baseImageUrl}${value.poster_path}`,
          title: value.title,
        }));
        setMovies([...movies, ...mappedMovies]);
      }
    } catch (error) {
      // Error handling. Ex: logger, alerting, etc.
      setError('Something went wrong!');
    }
  }
  return (
    <div className="relative">
      <div data-testid="section-hero">
        <Hero />
      </div>
      <FilterAndView />
      <div className="-mt-7 min-h-[20rem]" data-testid="section-view">
        {movies ? (
          view === "grid" ? (
            <div data-testid="" className="flex flex-col items-center justify-center">
              <MovieGrid movies={filteredMovies} />
            </div>
          ) : (
            view === "list" && (
              // <div data-testid="movie-list">
                <MovieList movies={filteredMovies} />
              // </div>
            )
          )
        ) : (
          <div className="flex justify-center items-center">
            <img src={`${process.env.PUBLIC_URL}/Spinner.gif`} alt="Loading" />
            <p>loading</p>
          </div>
        )}
      </div>
    </div>
  );
}

export default Home;

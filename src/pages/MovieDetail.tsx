import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { MovieDetailEntity } from '../entities/movieDetailEntity';
import { Movies } from '../entities/movie';
import MovieContent from "../components/MovieDetail/MovieContent";
import MovieGrid from '../components/Home/MovieGrid'

function MovieDetail () {
  const { id } = useParams()
  // state for top movies
  const [topMovies, setTopMovies] = useState<Movies>([])

  // state for movie detail
  const [movieDetail, setMovieDetail] = useState<MovieDetailEntity>()

  // Fetch movie
  useEffect(() => {
    fetchMovieDetail()
    fetchTopMovie()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id])

  // Read api url from env
  const baseUrl = process.env.REACT_APP_API_BASE_URL;
  const baseImageUrl = process.env.REACT_APP_IMAGE_BASE_URL;
  const apiToken = process.env.REACT_APP_API_TOKEN;
  
  // Header Auth 
  const headers = {
    Authorization: `Bearer ${apiToken}`,
  };
  
  // Fetch Movie Detail Data
  async function fetchMovieDetail() {
    try {
      // Fetching data
      const response = await fetch(
        `${baseUrl}/movie/${id}`, {headers}
      )
      const result = await response.json()

      // Map results to MovieDetail entity
      const mappedMovieDetail: MovieDetailEntity = {
        id: Number(result.id),
        backdrop_path: `${baseImageUrl}${result.backdrop_path}`,
        overview: result.overview,
        poster_path: `${baseImageUrl}${result.poster_path}`,
        title: result.title,
        genres: result.genres,
        runtime: result.runtime,
        release_date: result.release_date,
      };
      setMovieDetail(mappedMovieDetail)
    } catch (error) {
      // Error handling. Ex: logger, alerting, etc.
    } 
  }

  // Fetch Top Rated Movies Data
  async function fetchTopMovie() {
    try {
      // Fetching data
      const response = await fetch(
        `${baseUrl}/movie/top_rated?language=en-US&page=1`, {headers}
      )
      const result = await response.json()
      const limitedData = result.results.slice(0, 10);

      // Make sure there is at least have 1 result
      if (limitedData.length > 0) {
        // Map results to Movie entity
        const mappedTopMovies = limitedData.map((value: any) => ({
          id: Number(value.id),
          backdrop_path: `${baseImageUrl}${value.backdrop_path}`,
          original_title: value.original_title,
          overview: value.overview,
          poster_path: `${baseImageUrl}${value.poster_path}`,
          title: value.title
        }))
        setTopMovies([...mappedTopMovies])
      }

      window.scrollTo({ top: 0 })
    } catch (error) {
      // Error handling. Ex: logger, alerting, etc.
    } 
  }

  return (
    <div>
      {movieDetail ? (
        <>
          <MovieContent movieDetail={movieDetail}/>
          <div className="text-3xl text-white pl-14 pb-8 bg-black font-bold">Top Rated Movies You Might Like</div>
          <MovieGrid movies={topMovies} />
        </>
      ) : (
        <div className="h-screen flex items-center justify-center">
          <div>
            <img src={`${process.env.PUBLIC_URL}/Spinner.gif`} alt="Loading" />
            <p className="mt-2">Loading</p>
          </div>
        </div>
      )}
    </div>
  )
}

export default MovieDetail
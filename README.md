# Virtualspirit Code Challenge

This is to accomplish virtualspirit code challenge assessment by **Rifa Mukhlisa**.

- Email: [rifa.mukhlisa@gmail.com](mailto:rifa.mukhlisa@gmail.com)

## Run Locally

#### Prerequisites
- Node version >= 20.9.0

#### 1. Installation
```
cp env.sample .env && npm i
```

#### 2. Setup API Token
- Open your [TMDB developer account](https://developer.themoviedb.org/docs/authentication-application#api-key)
- Copy API Key to `.env` on this line:
```
REACT_APP_API_TOKEN=
```

#### 3. Run App
```
npm run start
```

then open `http://localhost:9001` from your browser

## Run Test
run this command in your terminal `npm test`

## Packages

- CSS framework: [tailwindcss](https://tailwindcss.com)
- Icon library: [lucide](https://lucide.dev)

## Writing Styles

- Javascript: [guideline](https://developer.mozilla.org/en-US/docs/MDN/Writing_guidelines/Writing_style_guide/Code_style_guide/JavaScript)
- Stylesheet: [guideline](https://en.bem.info/methodology/quick-start)

## Resources
- API: [TMDB ](https://themoviedb.org)
- Fonts: [Poppins](https://fonts.google.com/specimen/Poppins/about)
